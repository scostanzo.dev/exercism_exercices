import math
import random


class Character:
    def __init__(self):
        self.strength = self.ability()
        self.dexterity = self.ability()
        self.constitution = self.ability()
        self.intelligence = self.ability()
        self.wisdom = self.ability()
        self.charisma = self.ability()
        self.hitpoints = 10 + modifier(self.constitution)

    def ability(self):
        dice_results = sorted(random.randint(1, 6) for _ in range(4))
        del dice_results[0]
        return sum(dice_results) / len(dice_results)


def modifier(constitution_score):
    return math.floor((constitution_score - 10) / 2)
