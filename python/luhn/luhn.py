class Luhn:
    def __init__(self, card_num):
        self.card_num = card_num

    def valid(self):
        # remove empty spaces
        self.card_num = self.card_num.replace(" ", "")
        if not self.card_num.isdigit() or len(self.card_num) < 2:
            return False

        tot = 0
        for idx, num in enumerate(reversed(self.card_num)):
            # check every second digit starting from the right
            if idx % 2 == 1:
                partial = int(num) + int(num)
                if partial > 9:
                    tot += partial - 9
                else:
                    tot += partial
            else:
                tot += int(num)

        if tot % 10 == 0:
            return True
        return False


